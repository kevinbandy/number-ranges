type Range = [number, number];

type Serializer = (ranges: Array<Range>) => string

type ConfigOptions = {
    serializer?: Serializer,
}

const defaultSerializer: Serializer =
    (ranges: Array<Range>): string => ranges
        .map((range: Range) => `${range[0]}` + (range[0] === range[1] ? '' : `-${range[1]}`))
        .join(',');

export default class NumberRanges {
    private ranges: Array<Range>;

    constructor(input: Array<number>, config: ConfigOptions) {
        this.ranges = fromIterable(input);

        const serializer = (config && config.serializer instanceof Function) ? config.serializer : defaultSerializer;

        this.toString = () => serializer(this.ranges);
    }

    size(): number {
        return this.ranges.reduce(
            (sum: number, range: Range) => sum + (range[1] - range[0] + 1),
            0
        );
    }

    contains(needle: number): boolean {
        return this.ranges.reduce(
            (inRange: boolean, range: Range) => inRange || isInRange(needle, range),
            false
        );
    }

    toJSON(): string {
        return this.toString();
    }

    add(numbersToAdd: Iterable<number>): NumberRanges {
        const newNumbers: Array<number> = filter(number => !this.contains(number), numbersToAdd);

        if (newNumbers.length > 0) {
            const newRanges: Array<Range> = fromIterable(newNumbers);
            this.ranges = normalizeRanges(newRanges.reduce(function insertToRight(updatedRanges, newRange) {
                const insertAt = updatedRanges.reduce(function findLeft(left, range, leftIndex) {
                    if (range[0] < newRange[0]) {
                        return leftIndex + 1;
                    }

                    return left;
                }, 0);

                updatedRanges.splice(insertAt, 0, newRange);

                return updatedRanges;
            }, this.ranges));
        }

        return this;
    }

    remove(numbersToRemove: Iterable<number>): NumberRanges {
        const existingNumbers: Array<number> = filter(this.contains.bind(this), numbersToRemove);

        if (existingNumbers.length > 0) {
            this.ranges = this.ranges.reduce((updatedRanges: Array<Range>, range: Range): Array<Range> => {
                let rangeAsRanges: Array<Range> = [range];

                if (existingNumbers.indexOf(range[0]) > -1) {
                    range[0] = range[0] + 1;
                }

                if (existingNumbers.indexOf(range[1]) > -1) {
                    range[1] = range[1] - 1;
                }

                rangeAsRanges = existingNumbers.reduce((updatedRange: Array<Range>, existingNumber: number): Array<Range> => {
                    const matchedRange: Range = updatedRange.filter(isInRange.bind(null, existingNumber))[0];

                    if (matchedRange) {
                        const matchedRangeIndex: number = updatedRange.indexOf(matchedRange);
                        const newRanges: Array<Range> = [[matchedRange[0], existingNumber - 1], [existingNumber + 1, matchedRange[1]]];

                        updatedRange.splice(matchedRangeIndex, 1, newRanges[0], newRanges[1]);
                    }

                    return updatedRange;
                }, rangeAsRanges);

                return updatedRanges.concat(rangeAsRanges);
            }, []);
        }

        return this;
    }

    toArray(): Array<number> {
        return this.ranges.reduce(
            (target: Array<number>, range: Range) => target.concat(rangeToArray(range)),
            []
        );
    }
}

function fromIterable(numbers: Iterable<number>): Array<Range> {
    const naturalNumbers = Array.from(numbers);

    if (naturalNumbers.length === 0) {
        return [];
    }

    if (naturalNumbers.length === 1) {
        return [[naturalNumbers[0], naturalNumbers[0]]];
    }

    var sorted: Array<number> = naturalNumbers.slice(0).sort((a: number, b: number) => a - b);

    return sorted
        .slice(1)
        .reduce((range: Array<Array<number>>, item: number): Array<Array<number>> => {
            const rangedIndex: number = range.reduce((ranged: number, rangedCollection: Array<number>, rangedIndex: number) => {
                const isSelfOrPreviousPresent: boolean = rangedCollection.indexOf(item - 1) > -1 || rangedCollection.indexOf(item) > -1;

                return isSelfOrPreviousPresent ? rangedIndex : ranged;
            }, -1);

            if (rangedIndex > -1) {
                range[rangedIndex] = range[rangedIndex].concat(item);

                return range;
            }

            return range.concat([[item]]);
        }, [[sorted[0]]])
        .map((partitioned: Array<number>): Range => [partitioned[0], partitioned[partitioned.length - 1]]);
}

function normalizeRanges(ranges: Array<Range>): Array<Range> {
    type Continuation = { matches: Array<Range>, end: number | null } | null;

    let wasSubsumed: Array<Range> = [];

    return ranges.reduce((normalized: Array<Range>, range: Range, index: number) => {
        const continuedBy: Continuation = findContinuation(range, ranges.slice(index));

        if (wasSubsumed.indexOf(range) > -1) {
            return normalized;
        }

        if (continuedBy && continuedBy.end) {
            wasSubsumed = wasSubsumed.concat(continuedBy.matches);
            range[1] = continuedBy.end;
        }

        return normalized.concat([range]);
    }, []);

    function findContinuation(range: Range, ranges: Array<Range>): Continuation {
        var newRange = range.slice(0);

        return ranges.reduce(function buildContinuation(continuation: Continuation, candidate: Range) {
            var leftNumber = candidate[0] - 1;
            var isAfter = leftNumber <= newRange[1] && leftNumber >= newRange[0];

            if (isAfter) {
                if (!continuation) {
                    continuation = {
                        matches: [],
                        end: null
                    };
                }

                newRange[1] = candidate[1];
                continuation.matches.push(candidate);
                continuation.end = candidate[1];
            }


            return continuation;
        }, null);
    }
}

function isInRange(needle: number, range: Range): boolean {
    return needle >= range[0] && needle <= range[1];
}

function rangeToArray(range: Range): Array<number> {
    const returnValue: Array<number> = [];
    const start: number = range[0];
    const end: number = range[1];
    const length: number = range[1] - range[0] + 1;

    for (let i = 0; i < length; i++) {
        returnValue.push(start + i);
    }

    return returnValue;
}

function reduce(
    reducer: (accumulator: any, item: any) => any,
    accumulator: any,
    collection: Iterable<any>
): any {
    let returnValue: any = accumulator;

    for (let item of collection) {
        returnValue = reducer(returnValue, item);
    }

    return returnValue;
}

function filter<T>(
    predicate: (v: any) => boolean,
    collection: Iterable<T>
): Array<T> {
    return reduce(
        (acc: Array<T>, item: T): Array<T> => predicate(item) ? acc.concat(item) : acc,
        [],
        collection
    );
}

function first<T>(pair: [T, any]): T {
    return pair[0];
}

function second<T>(pair: [any, T]): T {
    return pair[1];
}
