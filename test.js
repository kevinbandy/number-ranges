import test from 'ava';

import NumberRanges from './';

var numbers = [0, 1, 2, 3, 5, 6];

// #size

test(
    '#size: with a non-empty input array',
    t => t.is(numbers.length, new NumberRanges(numbers).size())
);

test(
    '#size: with an empty input array',
    t => t.is(0, new NumberRanges([]).size())
);

// #contains

test(
    '#contains: when the range contains the number',
    t => t.true(new NumberRanges(numbers).contains(2))
);

test(
    '#contains: when the range does not contain the number',
    t => t.false(new NumberRanges(numbers).contains(7))
);

// #toString

test(
    '#toString: with zero elements',
    t => t.is('', new NumberRanges([]).toString())
);

test(
    '#toString: with a single element',
    t => t.is('0', new NumberRanges([0]).toString())
);

test(
    '#toString: with multiple elements in a single range',
    t => t.is('0-5', new NumberRanges([0, 1, 2, 3, 4, 5]).toString())
);

test(
    '#toString: with multiple elements in multiple ranges',
    t => t.is('0,2-4,9-12', new NumberRanges([0, 2, 3, 4, 9, 10, 11, 12]).toString())
);

test(
    '#toString: with multiple elements not in ranges',
    t => t.is('0,2,4,6,8,10', new NumberRanges([0, 2, 4, 6, 8, 10]).toString())
);

test(
    '#toString: with duplicates',
    t => t.is('0-2', new NumberRanges([0, 1, 2, 0, 2]).toString())
);

test(
    '#toString: serializes in JSON',
    t => t.is('{"range":"0-3,5-6"}', JSON.stringify({ range: new NumberRanges(numbers)}))
);

// Alternate serializer

const alternateSerializer = (rs) => rs.map(r => `${r[0]}:${r[1]}`).join('###');

test(
  'Alternate serializer, #toString',
  t => t.is('0:3###5:6', new NumberRanges(numbers, { serializer: alternateSerializer }).toString())
);

test(
  'Alternate serializer, JSON',
  t => t.is('{"range":"0:3###5:6"}', JSON.stringify({ range: new NumberRanges(numbers, { serializer: alternateSerializer }) }))
);

// #add

test(
    '#add: with no new numbers',
    t => t.is('0-3,5-6', new NumberRanges(numbers).add([0]).toString())
);

test(
    '#add: with one new number',
    t => t.is('0-6', new NumberRanges(numbers).add([4]).toString())
);

test(
    '#add: with many new numbers and ranges',
    t => t.is('0-6,10-12,18-20', new NumberRanges(numbers).add([4, 5, 6, 10, 11, 12, 18, 19, 20]).toString())
);

test(
    '#add: with out of order numbers',
    t => t.is('0-6,10-12,18-20', new NumberRanges(numbers).add([12, 5, 19, 11, 10, 4, 18, 6, 20]).toString())
);

// #remove

test(
    '#remove: with no overlapping numbers',
    t => t.is('0-3,5-6', new NumberRanges(numbers).remove([12]).toString())
);

test(
    '#remove: with a single number to remove',
    t => t.is('0-2,5-6', new NumberRanges(numbers).remove([3]).toString())
);

test(
    '#remove: with a multiple numbers to remove',
    t => t.is('0,2-3,5', new NumberRanges(numbers).remove([1, 6]).toString())
);

// With Iterables

test(
    'with Set',
    t => t.is('0-3,5-6', new NumberRanges(new Set(numbers)).toString())
);

test(
  'with TypedArray',
  t => t.is('0-3,5-6', new NumberRanges(new Uint8Array([0x00, 0x01, 0x02, 0x03, 0x05, 0x06])).toString())
);

// #toArray

test(
  '#toArray: with empty NumberRanges',
  t => t.deepEqual([], new NumberRanges([]).toArray())
);

test(
  '#toArray: with non-empty NumberRanges',
  t => t.deepEqual(numbers, new NumberRanges(numbers).toArray())
);

test(
  '#toArray: with messy numbers',
  t => t.deepEqual([0, 1, 2, 3, 5, 6], new NumberRanges([6, 5, 3, 2, 1, 0]).toArray())
);
