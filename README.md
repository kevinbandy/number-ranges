# Number Ranges

Library for working with sets of natural numbers and serializing as range strings.

## Example

```js
import NumberRanges from 'number-ranges';

// Constructor
var myRange = new NumberRanges([0, 1, 2, 3, 5, 6]);

// String/JSON representation
console.log(myRange); // 0-3,5-6
console.log(myRange.toString()); // '0-3,5-6'
console.log(JSON.stringify({ range: myRange })); // {"range":"0-3,5-6"}

// Add items to the set
myRange.add([4]);
myRange.add([4]);
myRange.add([4]);
myRange.add(new Set([4])); // Works for TypedArray and ES2015 Set

console.log(myRange); // 0-6

// Remove items from the set
myRange.remove([4, 1, 6]);

console.log(myRange); // 1,2-3,5

// Check for set membership
console.log(myRange.contains(2)); // true
console.log(myRange.contains(4)); // false

// Alternate Serializer
var alternateSerializer = function alternateSerializer(ranges) {
    return ranges
        .map(function (range) {
            return '' + range[0] + ':'
        })
        .join('###');
}
var mySpecialRange = new NumberRanges([0, 1, 2, 3, 5, 6], { serializer: alternateSerializer });

console.log(mySpecialRange.toString()); // '0:3###5:6'

// Covert back to Array of (sorted) numbers
console.log(myRange.toArray()); // [0, 1, 2, 3, 5, 6]
```

## Roadmap

- [ ] API Docs
- [ ] More set operations (intersection, difference, et. al.)
- [X] Operate on ES2015 Sets
- [X] `.toArray`
